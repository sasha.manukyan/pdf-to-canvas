import subprocess

def run_simplify(svg_path):
    result = subprocess.run(['svgo', svg_path, '-o' 'output.svg'], capture_output=True, text=True)
    if result.returncode != 0:
        print("Error:", result.stderr)