from xml.sax.saxutils import escape
from hepler import rgb_to_hex
from lxml import etree as ET
import re


#### Creating text and image #####

def create_svg_text_element(pdf_object):
    x = pdf_object['position']['x0']
    y = pdf_object['position']['y1'] 
    content = escape(pdf_object['content'])
    fill = rgb_to_hex(pdf_object['color'])
    font_size = pdf_object['size']

    return f'<text x="{x}" y="{y}" font-family="{pdf_object["font"]}" font-size="{font_size}" fill="{fill}">{content}</text>'

def create_svg_image_element(image_info):
    a = f'<image x="{image_info["transform"]["x"]}" y="{image_info["transform"]["y"]}" width="{image_info["scale"]["width"]}" height="{image_info["scale"]["height"]}" href="images/{image_info["image_xref"]}" />'
    return a

def create_svg(content):
    result = str()
    svg_header = '<svg xmlns="http://www.w3.org/2000/svg" width="1000" height="1000">\n <rect width="1000" height="1000" fill="white"/>\n'
    
    svg_footer = '</svg>'

    result += svg_header
    result += content
    result += svg_footer

    return result


###

def parse_transform(transform):
    """Extracts the translation values from a transform string."""
    if "translate" in transform:
        numbers = re.findall(r"[-+]?\d*\.\d+|\d+", transform)
        return tuple(map(float, numbers))
    return (0, 0)  # Default to no translation

def apply_translate(path, translate):
    # Split the path data into commands and parameters
    parts = re.split('([a-zA-Z])', path)
    if not translate:
        return path

    x, y = translate
    new_parts = []

    # Iterate through the parts to apply translation
    for i, part in enumerate(parts):
        if part in ['M', 'L', 'T']:  # Add other commands as needed
            # The next part is expected to be the coordinates
            coords = parts[i + 1].split(',')
            if len(coords) == 2:
                new_x = str(float(coords[0]) + x)
                new_y = str(float(coords[1]) + y)
                new_coords = ','.join([new_x, new_y])
                new_parts.append(part + new_coords)
                continue
        new_parts.append(part)

    return ''.join(new_parts)

def extract_style_to_attributes(svg_content):
    # Parse the SVG content
    root = ET.fromstring(svg_content)

    # Iterate over all <path> elements
    for path in root.iter('{http://www.w3.org/2000/svg}path'):
        style = path.get('style')
        if style:
            # Parse the style attribute
            style_attrs = dict(item.split(":") for item in style.split(";") if item)
            
            # Set each style property as an attribute of the path
            for key, value in style_attrs.items():
                path.set(key, value.strip())

            # Remove the original style attribute
            del path.attrib['style']

    return ET.tostring(root, encoding='unicode')

def parse_style(style):
    """Parses the style attribute to extract fill and fill-opacity."""
    style_dict = {}
    if style:
        style_parts = style.split(';')
        for part in style_parts:
            if ':' in part:
                key, value = part.split(':')
                style_dict[key.strip()] = value.strip()
    return style_dict


####### Removing texts & image from svg #######

def remove_texts_images_and_labeled_paths(svg_content, output_file):
    # Convert the SVG content to a byte string if it's not already
    if isinstance(svg_content, str):
        svg_content = svg_content.encode('utf-8')

    # Parse the SVG content
    root = ET.fromstring(svg_content)

    # Define the namespace dictionary
    namespaces = {'svg': 'http://www.w3.org/2000/svg'}

    # Removing all <text> elements
    for text_element in root.xpath('.//svg:text', namespaces=namespaces):
        parent = text_element.getparent()
        if parent is not None:
            parent.remove(text_element)

    # Removing all <image> elements
    for image_element in root.xpath('.//svg:image', namespaces=namespaces):
        parent = image_element.getparent()
        if parent is not None:
            parent.remove(image_element)

    # Removing all <path> elements with an 'aria-label' attribute
    for path_element in root.xpath('.//svg:path[@aria-label]', namespaces=namespaces):
        parent = path_element.getparent()
        if parent is not None:
            parent.remove(path_element)

    # Write the modified SVG to a file
    tree = ET.ElementTree(root)
    tree.write(output_file, encoding='utf-8', xml_declaration=True)


######### Getting path from svg #######
    
def apply_translate_to_path(d, translate):
    # Split the path data into commands and parameters
    parts = re.split('([a-zA-Z])', d)
    x, y = translate
    new_parts = []

    # Iterate through the parts to apply translation
    for i, part in enumerate(parts):
        if part in ['M', 'L', 'T']:  # Add other commands as needed
            # The next part is expected to be the coordinates
            coords = parts[i + 1].split(',')
            if len(coords) == 2:
                new_x = str(float(coords[0]) + x)
                new_y = str(float(coords[1]) + y)
                new_coords = ','.join([new_x, new_y])
                new_parts.append(part + new_coords)
                continue
        new_parts.append(part)

    return ''.join(new_parts)

def parse_transform(transform):
    # Extract translation values from the transform attribute.
    if "translate" in transform:
        numbers = re.findall(r"[-+]?\d*\.\d+|\d+", transform)
        return tuple(map(float, numbers))
    return (0, 0)

def simplify_svg(svg_content):
    # Convert the SVG content to a byte string if it's not already
    if isinstance(svg_content, str):
        svg_content = svg_content.encode('utf-8')

    root = ET.fromstring(svg_content)
    namespaces = {'svg': 'http://www.w3.org/2000/svg'}

    all_paths = []

    # Process each group
    for group in root.xpath('.//svg:g', namespaces=namespaces):
        transform = group.attrib.get('transform', '')
        translate = parse_transform(transform)

        for element in list(group):
            if element.tag.endswith('path'):
                d = element.attrib['d']
                transformed_d = apply_translate_to_path(d, translate)
                element.attrib['d'] = transformed_d
                all_paths.append(element)

    # Remove all groups and add back all processed paths
    for group in root.xpath('.//svg:g', namespaces=namespaces):
        group.getparent().remove(group)
    
    for path in all_paths:
        root.append(path)

    return ET.tostring(root, encoding='unicode')


def add_svg_element(svg_content, new_element_string):
    # Parse the existing SVG content
    parser = ET.XMLParser(encoding="utf-8")
    root = ET.fromstring(svg_content.encode('utf-8'), parser=parser)

    # # Parse the new element string
    new_element = ET.fromstring(new_element_string.encode('utf-8'), parser=parser)

    # Append the new element to the root (SVG element)
    root.append(new_element)

    # # Convert the modified tree back to a string
    new_svg_content = ET.tostring(root, encoding='unicode')

    return new_svg_content