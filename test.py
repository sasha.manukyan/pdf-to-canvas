# import xml.etree.ElementTree as ET
# import re


# def extract_text_from_svg(svg_file_path):
#     # Parse the SVG file
#     tree = ET.parse(svg_file_path)
#     root = tree.getroot()

#     # Namespace handling for SVG files
#     namespaces = {'svg': 'http://www.w3.org/2000/svg'}

#     # Extract path elements with aria-label
#     texts = []
#     for path in root.findall('.//svg:path', namespaces):
#         aria_label = path.get('aria-label')
#         if aria_label:
#             style = path.get('style')
#             transform = path.get('transform')

#             # Extract color from style
#             color_match = re.search(r'fill:(#[0-9a-fA-F]+)', style)
#             color = color_match.group(1) if color_match else None

#             # Extract transform matrix and get x, y positions
#             transform_match = re.search(r'matrix\(([^)]+)\)', transform)
#             if transform_match:
#                 matrix_values = transform_match.group(1).split(' ')
#                 x_position = float(matrix_values[4])
#                 y_position = float(matrix_values[5])
#             else:
#                 x_position, y_position = None, None

#             texts.append({
#                 'text': aria_label,
#                 'color': color,
#                 'x_position': x_position,
#                 'y_position': y_position
#             })

#     return texts

# print(extract_text_from_svg('simply2.svg'))

import requests

# c = "<svg baseProfile=\"full\" height=\"50px\" version=\"1.1\" width=\"320px\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:ev=\"http://www.w3.org/2001/xml-events\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><defs><style type=\"text/css\" /><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: Arial;\n                    src: url(https://pastatic.picsart.com/63267477329311766066.ttf);\n                }\n            ]]></style><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: LEMON MILK Pro Heavy;\n                    src: url(https://project-files.picsart.com/project_files/e2cbc612-7779-4c57-b260-fab987afaff2.otf);\n                }\n            ]]></style><linearGradient id=\"linearGradient\" x1=\"0.6237185002524486\" x2=\"0.3762814997475513\" y1=\"0.015548007852909274\" y2=\"0.9844519921470907\"><stop offset=\"0\" stop-color=\"#8e2d09\" /><stop offset=\"1\" stop-color=\"#ff7f50\" /></linearGradient><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: Arial;\n                    src: url(https://pastatic.picsart.com/63267477329311766066.ttf);\n                }\n            ]]></style></defs><image id=\"background\" height=\"50\" preserveAspectRatio=\"xMidYMid slice\" width=\"320\" xlink:href=\"https://dcdn.picsart.com/dev/2a668fde-8e12-41ca-b55e-9929ce94cccf.png\" />\n            <image height=\"50.0\" opacity=\"100\" preserveAspectRatio=\"xMidYMid slice\" transform=\"rotate(0 160.0 25.0)\" width=\"320.0\" x=\"0.0\" xlink:href=\"https://dcdn.picsart.com/dev/83ec2564-be46-4a13-834a-5234e8fb7ff1.png\" y=\"0.0\" />\n            <image width=\"96.58331515774734\" height=\"42.062042838210196\" opacity=\"100\" transform=\"rotate(0 53.9013080885131 24.99999999999985)\"  x=\"5.60965050963943\" xlink:href=\"https://dcdn.picsart.com/dev/2a668fde-8e12-41ca-b55e-9929ce94cccf.png\" y=\"3.9689785808947526\" />\n            <text fill=\"#ffffff\" font-family=\"Arial\" font-size=\"12\" font-weight=\"bold\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(10 149.345702875 19.000000375)\" x=\"149.345702875\" y=\"19.000000375\">Find Your Inner Balance</text>\n            <text fill=\"#ffffff\" font-family=\"LEMON MILK Pro Heavy\" font-size=\"10\" font-weight=\"normal\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(0 150.9560543125 34.9999998125)\" x=\"150.9560543125\" y=\"34.9999998125\">Experience\n Peace and Serenity</text>\n <path d=\"m5 0h50c5 0 5 5 5 5v10c0 5 -5 5 -5 5h-50c-5 0 -5 -5 -5 -5v-10c0 -5 5 -5 5 -5z\" fill=\"url(#linearGradient)\" opacity=\"100\" transform=\"rotate(-14.32589875190922 280.0000000000002 25.0000000000003) translate(250.00000000000023,15.000000000000298)\" />\n <text fill=\"#ffffff\" font-family=\"Arial\" font-size=\"10\" font-weight=\"normal\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(0 280.2119139375 29.5000003125)\" x=\"280.2119139375\" y=\"29.5000003125\">Book Now</text>\n </svg>"

content = str()
with open("cats.svg", "r") as file:
    content = str(file.read())

print(content)



# content = "<svg baseProfile=\"full\" height=\"50px\" version=\"1.1\" width=\"320px\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:ev=\"http://www.w3.org/2001/xml-events\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><defs><style type=\"text/css\" /><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: Arial;\n                    src: url(https://pastatic.picsart.com/63267477329311766066.ttf);\n                }\n            ]]></style><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: LEMON MILK Pro Heavy;\n                    src: url(https://project-files.picsart.com/project_files/e2cbc612-7779-4c57-b260-fab987afaff2.otf);\n                }\n            ]]></style><linearGradient id=\"linearGradient\" x1=\"0.6237185002524486\" x2=\"0.3762814997475513\" y1=\"0.015548007852909274\" y2=\"0.9844519921470907\"><stop offset=\"0\" stop-color=\"#8e2d09\" /><stop offset=\"1\" stop-color=\"#ff7f50\" /></linearGradient><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: Arial;\n                    src: url(https://pastatic.picsart.com/63267477329311766066.ttf);\n                }\n            ]]></style></defs><image id=\"background\" height=\"50\" preserveAspectRatio=\"xMidYMid slice\" width=\"320\" xlink:href=\"https://dcdn.picsart.com/dev/2a668fde-8e12-41ca-b55e-9929ce94cccf.png\" />\n            <image height=\"50.0\" opacity=\"100\" preserveAspectRatio=\"xMidYMid slice\" transform=\"rotate(0 160.0 25.0)\" width=\"320.0\" x=\"0.0\" xlink:href=\"https://dcdn.picsart.com/dev/83ec2564-be46-4a13-834a-5234e8fb7ff1.png\" y=\"0.0\" />\n            <image width=\"96.58331515774734\" height=\"42.062042838210196\" opacity=\"100\" transform=\"rotate(0 53.9013080885131 24.99999999999985)\"  x=\"5.60965050963943\" xlink:href=\"https://dcdn.picsart.com/dev/2a668fde-8e12-41ca-b55e-9929ce94cccf.png\" y=\"3.9689785808947526\" />\n            <text fill=\"#ffffff\" font-family=\"Arial\" font-size=\"12\" font-weight=\"bold\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(10 149.345702875 19.000000375)\" x=\"149.345702875\" y=\"19.000000375\">Find Your Inner Balance</text>\n            <text fill=\"#ffffff\" font-family=\"LEMON MILK Pro Heavy\" font-size=\"10\" font-weight=\"normal\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(0 150.9560543125 34.9999998125)\" x=\"150.9560543125\" y=\"34.9999998125\">Experience\n Peace and Serenity</text>\n <path d=\"m5 0h50c5 0 5 5 5 5v10c0 5 -5 5 -5 5h-50c-5 0 -5 -5 -5 -5v-10c0 -5 5 -5 5 -5z\" fill=\"url(#linearGradient)\" opacity=\"100\" transform=\"rotate(-14.32589875190922 280.0000000000002 25.0000000000003) translate(250.00000000000023,15.000000000000298)\" />\n <text fill=\"#ffffff\" font-family=\"Arial\" font-size=\"10\" font-weight=\"normal\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(0 280.2119139375 29.5000003125)\" x=\"280.2119139375\" y=\"29.5000003125\">Book Now</text>\n </svg>"

resp = requests.post('http://127.0.0.1:8000/svg-import-miniapp', json={"svg_content": "<svg baseProfile=\"full\" height=\"50px\" version=\"1.1\" width=\"320px\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:ev=\"http://www.w3.org/2001/xml-events\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><defs><style type=\"text/css\" /><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: Arial;\n                    src: url(https://pastatic.picsart.com/63267477329311766066.ttf);\n                }\n            ]]></style><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: LEMON MILK Pro Heavy;\n                    src: url(https://project-files.picsart.com/project_files/e2cbc612-7779-4c57-b260-fab987afaff2.otf);\n                }\n            ]]></style><linearGradient id=\"linearGradient\" x1=\"0.6237185002524486\" x2=\"0.3762814997475513\" y1=\"0.015548007852909274\" y2=\"0.9844519921470907\"><stop offset=\"0\" stop-color=\"#8e2d09\" /><stop offset=\"1\" stop-color=\"#ff7f50\" /></linearGradient><style type=\"text/css\"><![CDATA[\n                @font-face {\n                    font-family: Arial;\n                    src: url(https://pastatic.picsart.com/63267477329311766066.ttf);\n                }\n            ]]></style></defs><image id=\"background\" height=\"50\" preserveAspectRatio=\"xMidYMid slice\" width=\"320\" xlink:href=\"https://dcdn.picsart.com/dev/2a668fde-8e12-41ca-b55e-9929ce94cccf.png\" />\n            <image height=\"50.0\" opacity=\"100\" preserveAspectRatio=\"xMidYMid slice\" transform=\"rotate(0 160.0 25.0)\" width=\"320.0\" x=\"0.0\" xlink:href=\"https://dcdn.picsart.com/dev/83ec2564-be46-4a13-834a-5234e8fb7ff1.png\" y=\"0.0\" />\n            <image width=\"96.58331515774734\" height=\"42.062042838210196\" opacity=\"100\" transform=\"rotate(0 53.9013080885131 24.99999999999985)\"  x=\"5.60965050963943\" xlink:href=\"https://dcdn.picsart.com/dev/2a668fde-8e12-41ca-b55e-9929ce94cccf.png\" y=\"3.9689785808947526\" />\n            <text fill=\"#ffffff\" font-family=\"Arial\" font-size=\"12\" font-weight=\"bold\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(10 149.345702875 19.000000375)\" x=\"149.345702875\" y=\"19.000000375\">Find Your Inner Balance</text>\n            <text fill=\"#ffffff\" font-family=\"LEMON MILK Pro Heavy\" font-size=\"10\" font-weight=\"normal\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(0 150.9560543125 34.9999998125)\" x=\"150.9560543125\" y=\"34.9999998125\">Experience\n Peace and Serenity</text>\n <path d=\"m5 0h50c5 0 5 5 5 5v10c0 5 -5 5 -5 5h-50c-5 0 -5 -5 -5 -5v-10c0 -5 5 -5 5 -5z\" fill=\"url(#linearGradient)\" opacity=\"100\" transform=\"rotate(-14.32589875190922 280.0000000000002 25.0000000000003) translate(250.00000000000023,15.000000000000298)\" />\n <text fill=\"#ffffff\" font-family=\"Arial\" font-size=\"10\" font-weight=\"normal\" opacity=\"100\" text-anchor=\"middle\" text-decoration=\"none\" transform=\"rotate(0 280.2119139375 29.5000003125)\" x=\"280.2119139375\" y=\"29.5000003125\">Book Now</text>\n </svg>"})
print(resp)