import svgwrite


################




###############



pdf_path = 'bb.pdf'  
pdf_objects = extract_text(pdf_path)
pdf_image = extract_images(pdf_path)
print(pdf_image)
# Generating SVG content
svg_content = '\n'.join(create_svg_image_element(obj) for obj in pdf_image)
svg_content += '\n'.join(create_svg_text_element(obj) for obj in pdf_objects)
print(svg_content)
svg_output = svg_header + '\n' + svg_content + '\n' + svg_footer

with open('output.svg', 'w') as file:
    file.write(svg_output)


# Example usage
# pdf_file_path = 'illustratortest image.pdf'
# extracted_images = extract_images(pdf_file_path)
# print(extracted_images)
# print(extract_text)
    


# def convert_pdf_pages_to_images(pdf_path, output_folder, dpi=300):
#     # Open the PDF file
#     doc = fitz.open(pdf_path)

#     # Iterate through each page
#     for page_num in range(len(doc)):
#         page = doc.load_page(page_num)

#         # Render page to a pixmap (raster image)
#         pix = page.get_pixmap(matrix=fitz.Matrix(dpi / 72, dpi / 72))

#         # Save the pixmap as a PNG file
#         image_filename = f"{output_folder}/page_{page_num}.png"
#         pix.save(image_filename)

#         print(f"Saved page {page_num} as image to {image_filename}")

#     # Close the document
#     doc.close()

# convert_pdf_pages_to_images('chlp.pdf', './')