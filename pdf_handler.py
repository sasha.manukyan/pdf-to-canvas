import fitz
import pdfreader

def extract_shapes(pdf_path):
    doc = fitz.open(pdf_path)
    shapes = []
    for page_num in range(1):
        page = doc.load_page(page_num)
        content = page.get_contents()
        shapes.append(content)
    return shapes


def extract_images(pdf_path):
    doc = fitz.open(pdf_path)
    res_list = []

    page = doc.load_page(0)
    image_list = page.get_images(full=True)

    for i in range(len(image_list)):
        xref = image_list[i][0]
        base_img = doc.extract_image(xref)
        image_bytes = base_img["image"]
        image_ext = base_img["ext"]
        img_sec_info = page.get_image_info()[i]
        bbox = img_sec_info["bbox"] 

        with open(f"images/image{xref}.{image_ext}", "wb") as img_file:
                img_file.write(image_bytes)



        image_info = {
            "image_xref": f"image{xref}.{image_ext}",
            "scale": {
                "width": bbox[2] - bbox[0],
                "height": bbox[3] - bbox[1]
            },
            "transform": {
                "x": bbox[0],
                "y": bbox[1],
                "rotation": 0
            }
        }

        res_list.append(image_info)

    return res_list


def extract_text(pdf_path):
    doc = fitz.open(pdf_path)
    elements = []

    for page_number in range(1):
        page = doc.load_page(page_number)

        text_blocks = page.get_text("dict")["blocks"]
        for block in text_blocks:
            if 'lines' in block:  
                for line in block["lines"]:
                    for span in line["spans"]:
                        font = span["font"]
                        size = span["size"]
                        text = span["text"]
                        color = span["color"] 
                        bbox = span["bbox"]
                        # print("Text info is --->", span)  
                        # rot = span["dir"]
                        elements.append({
                            "type": "text",
                            "position": {"x0": bbox[0], "y0": bbox[1], "x1": bbox[2], "y1": bbox[3]},
                            "content": text,
                            "font": font,
                            "size": size,
                            "color": color 
                        })

    doc.close()
    return elements


# def remove_images_from_pdf(input_pdf, output_pdf):
#     doc = fitz.open(input_pdf)
#     for page in doc:
#         # Get list of image dictionaries on the page
#         image_list = page.get_images(full=True)

#         for img in image_list:
#             xref = img[0]  # xref is the reference number of the image
#             img_rects = page.get_image_rects(xref)

#             for rect in img_rects:
#                 # Cover the image area with a white rectangle
#                 page.add_redact_annot(rect, fill=[1, 1, 1])
#                 page.apply_redactions()

#     doc.save(output_pdf)
#     doc.close()


def remove_content_from_pdf(input_pdf, output_pdf):
    doc = fitz.open(input_pdf)
    for page in doc:
        # Cover images
        image_list = page.get_images(full=True)
        for img in image_list:
            xref = img[0]
            img_rects = page.get_image_rects(xref)
            for rect in img_rects:
                page.add_redact_annot(rect, fill=[1, 1, 1])

        # Cover text
        text_blocks = page.get_text("dict")["blocks"]
        for block in text_blocks:
            if "lines" in block:  # Check if it's a text block
                for line in block["lines"]:
                    for span in line["spans"]:
                        rect = fitz.Rect(span['bbox'])
                        page.add_redact_annot(rect, fill=[1, 1, 1])

        # Apply redactions
        page.apply_redactions()

    doc.save(output_pdf)
    doc.close()

def pdf_page_to_image(pdf_path, image_path, page_number=0):
    doc = fitz.open(pdf_path)
    page = doc.load_page(page_number) 

    pix = page.get_pixmap()

    pix.save(image_path)

    doc.close()

def remove_text_and_images(pdf_path, output_path):
    # Open the PDF
    doc = fitz.open(pdf_path)

    for page_num, page in enumerate(doc):
        # List of items to retain
        retain_items = []

        # Process images - assuming they are separate from other content
        for img in page.get_images(full=True):
            xref = img[0]
            page.clean_contents()  # Clean the page contents
            doc._deleteObject(xref)  # Delete the image object

        # Process text - try to be more selective
        blocks = page.get_text("dict")["blocks"]
        for block in blocks:
            if block["type"] == 0:  # Text block
                # Add your logic here to decide whether to remove the text block
                # For example, check its position, size, etc.
                pass
            else:
                # Non-text block, retain it
                retain_items.append(block)

        # TODO: Logic to rebuild the page using retain_items if necessary

    # Save the modified PDF
    doc.save(output_path)
    doc.close()


def extract_vectors(pdf_path, output_path):
    doc = fitz.open(pdf_path)
    for page_num in range(len(doc)):
        page = doc.load_page(page_num)
        contents = page.get_contents()  # Get the raw content of the page

        # Save the raw content to a file
        with open(f"{output_path}_page_{page_num + 1}.txt", 'w') as f:
            if isinstance(contents, list):
                for content in contents:
                    f.write(str(content))  # Convert to string before writing
            else:
                f.write(str(contents)) 


def get_pdf_dimensions(pdf_path):
    dimensions = []

    with open(pdf_path, "rb") as fd:
        reader = pdfreader.PDFDocument(fd)
            # Page MediaBox format: [llx, lly, urx, ury]
        media_box = next(reader.pages()).MediaBox
        width = media_box[2] - media_box[0]
        height = media_box[3] - media_box[1]
        dimensions = (width, height)

    return dimensions