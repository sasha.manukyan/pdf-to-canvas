import pdf_handler
import svg_gen
from hepler import get_svg_dim, resize_svg

path = 'pdfillustrator.pdf'

content = str()
vector_content = str()
result_content = str()
with open("pdfillustrator.svg", "r") as file:
    content = file.read()

svg_gen.remove_texts_images_and_labeled_paths(content, "tmp.svg")

with open("tmp.svg", "r") as file:
    vector_content = file.read()

images_content = pdf_handler.extract_images(path)
texts_content = pdf_handler.extract_text(path)

pdf_dim = pdf_handler.get_pdf_dimensions(path)


# vector_content = svg_gen.simplify_svg(vector_content)

vector_content = svg_gen.extract_style_to_attributes(vector_content)
print("hello", pdf_dim)
svg_dim = get_svg_dim(content)

print(svg_dim)

vector_content = resize_svg(vector_content, pdf_dim[0], pdf_dim[1])

width_scale = float(svg_dim[0]) / float(pdf_dim[0])
height_scale = float(svg_dim[1]) / float(pdf_dim[1])

for image in images_content:
    image['scale']['width'] *= width_scale
    image['scale']['height'] *= height_scale
    vector_content = svg_gen.add_svg_element(vector_content, svg_gen.create_svg_image_element(image))

for text in texts_content:
    vector_content = svg_gen.add_svg_element(vector_content, svg_gen.create_svg_text_element(text))


with open("res.svg", "w") as file:
    file = file.write(vector_content)