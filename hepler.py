import base64
from io import BytesIO
from xml.etree import ElementTree as ET
from PIL import Image

def int_to_rgb(color_int):
    red = (color_int >> 16) & 255
    green = (color_int >> 8) & 255
    blue = color_int & 255
    return (red, green, blue)

def rgb_to_hex(rgb):
    tmp_value = int_to_rgb(rgb)
    return "#{:02x}{:02x}{:02x}".format(*tmp_value)

def get_svg_dim(svg_content):
    # Option 1: Remove XML declaration (if present)
    if svg_content.startswith('<?xml'):
        svg_content = svg_content.split('?>', 1)[1]

    # Option 2: Convert to bytes
    # svg_content = svg_content.encode('utf-8')

    try:
        root = ET.fromstring(svg_content)
        width = root.attrib.get('width')
        height = root.attrib.get('height')
        return width, height
    except ET.ParseError as e:
        print(f"Error parsing SVG: {e}")
        return None, None

def resize_svg(svg_content, new_width, new_height):
    root = ET.fromstring(svg_content)

    root.set('width', str(new_width))
    root.set('height', str(new_height))

    return ET.tostring(root, encoding='unicode')

def extract_and_generate_images_from_svg(svg_content):
    # Parse the SVG content
    root = ET.fromstring(svg_content)

    # Namespace dictionary to handle SVG namespaces in the XML
    namespaces = {'svg': 'http://www.w3.org/2000/svg'}

    # Find all image elements
    images = root.findall('.//svg:image', namespaces)

    for i, image in enumerate(images):
        href = image.get('{http://www.w3.org/1999/xlink}href')

        # Check if the href is base64 encoded
        if href.startswith('data:image'):
            # Extracting the base64 part
            format, imgstr = href.split(';base64,') 
            ext = format.split('/')[-1] # Extracting the image extension

            # Decode the base64 string to bytes
            imgdata = base64.b64decode(imgstr)

            # Create an image from the byte data
            image = Image.open(BytesIO(imgdata))

            # Save the image
            filename = f'extracted_image_{i}.{ext}'
            image.save(filename)
            print(f'Saved image: {filename}')

content = str()

hrefs = {'https://dcdn.picsart.com/dev/05d13478-edfe-4126-9fde-3257fbb1168d.png',

'https://dcdn.picsart.com/dev/18c677d9-e298-42cb-90fe-b56f0e8d9106.png',

'https://dcdn.picsart.com/dev/e831143e-4d9a-4b18-97b7-a753541576e0.png',

'https://dcdn.picsart.com/dev/9d671961-a159-4186-a5c0-9cc6e084e67e.png',

'https://dcdn.picsart.com/dev/520408db-0758-4670-b35a-d22ff3e15e2c.png',

'https://dcdn.picsart.com/dev/a043a072-b35b-49f4-8848-e85f173f8bb3.png',

'https://dcdn.picsart.com/dev/515cf40c-31a6-4392-838f-018524080d03.png',

'https://dcdn.picsart.com/dev/d229ae8b-79fb-4bbd-bd04-5ce8bf6e8eb0.png',

'https://dcdn.picsart.com/dev/518579d3-d56a-4b72-858d-5cb7ec16abab.png',

'https://dcdn.picsart.com/dev/00d23954-506b-475a-93e6-73a665103aa2.png',

'https://dcdn.picsart.com/dev/14e6ae91-944c-4ea7-bd55-465d0bd5740d.png',

'https://dcdn.picsart.com/dev/54b9369d-6afc-47e2-9c9d-aba561c3e618.png',

'https://dcdn.picsart.com/dev/5f08f200-336c-4b89-8990-7eb1765374c0.png',

'https://dcdn.picsart.com/dev/8f184552-e0d2-4779-8a57-9d5df5a6dd43.png',

'https://dcdn.picsart.com/dev/555f2b83-618c-4db2-8bc3-172e5669d58d.png',

'https://dcdn.picsart.com/dev/91c32246-0423-45dd-9bec-b66f50eb4dd0.png'
}

with open('simply.svg', 'r') as file:
    content = file.read()

# extract_and_generate_images_from_svg(content)


def replace_image_links_in_svg(svg_content, new_urls):
    # Parse the SVG content
    root = ET.fromstring(svg_content)

    # Namespace dictionary to handle SVG namespaces in the XML
    namespaces = {'svg': 'http://www.w3.org/2000/svg', 'xlink': 'http://www.w3.org/1999/xlink'}

    # Find all image elements
    images = root.findall('.//svg:image', namespaces)

    # Iterate over images and replace the href with new URLs
    for image, new_url in zip(images, new_urls):
        image.set('{http://www.w3.org/2000/xlink}href', new_url)

    # Return the modified SVG content as a string
    return ET.tostring(root, encoding='unicode')

content = replace_image_links_in_svg(content, hrefs)

with open('aaaaa.svg', 'w') as file:
    file.write(content)